//
// Created by Landon on 2020-01-20.
//

#include <stdlib.h>

#include "vectorMath.h"

Vector *vectorMath(const Vector v1, const Vector v2, Vector *result, const char op) {
    // Check vector lengths
    if (v1.length != v2.length) return NULL;

    result->length = v1.length;

    switch (op) {
        case '+':
            /*
             * Add components
             */
            for (int i = 0; i < v1.length; i++) {
                result->components[i] = v1.components[i] + v2.components[i];
            }

            break;

        case '-':
            /*
             * Subtract components
             */
            for (int i = 0; i < v1.length; i++) {
                result->components[i] = v1.components[i] - v2.components[i];
            }

            break;

        case '*':
            /*
             * Multiply components
             */
            for (int i = 0; i < v1.length; i++) {
                result->components[i] = v1.components[i] * v2.components[i];
            }

            break;

        case '/':
            /*
             * Divide components
             */
            for (int i = 0; i < v1.length; i++) {
                result->components[i] = v1.components[i] / v2.components[i];
            }

            break;

        default:
            return NULL;
    }

    return result;
}

void printVector(const Vector v, FILE *stream) {
    for (int i = 0; i < v.length; i++) {
        fprintf(stream, WORD_FORMAT, v.components[i]);
    }

    fputs("\n", stream);
}
