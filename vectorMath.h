//
// Created by Landon on 2020-01-20.
//

#ifndef COMP_166_LAB8_VECTORMATH_H
#define COMP_166_LAB8_VECTORMATH_H

#include <stdio.h>

#define MAX_VECTOR_SIZE 1024

#ifndef WORD
#define WORD double
#endif

#ifndef WORD_FORMAT
#define WORD_FORMAT "%10.4lf "
#endif

typedef struct {
    long length;
    WORD components[MAX_VECTOR_SIZE];
} Vector;

Vector *vectorMath(const Vector v1, const Vector v2, Vector *result, const char op);

void printVector(const Vector v, FILE *stream);

#endif //COMP_166_LAB8_VECTORMATH_H
